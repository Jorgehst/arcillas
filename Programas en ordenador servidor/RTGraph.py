import os
import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import time

#Gráfica RT
app = QtGui.QApplication([])
win = pg.GraphicsWindow(title="Evolucion conductividad en el tiempo")
p = win.addPlot(title="Evolucion conductividad en el tiempo")
curva = p.plot(pen='y')
p.setRange(yRange=[0, 2000])

moisture_list = []
temperature_list =  []
conductivity_list = []
salinity_list = []
#Introucir el número de características
numeroCarateristicas = 3
i = 0
try:
    file_name = input("Introzca el nombre del archivo: ")
        

    def readFile(fileName):
        fileData = open(fileName, "r") #opens the file in read mode
        data_list = fileData.read().splitlines() #puts the file into an array
        fileData.close()
        return data_list


    ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name) + '.txt') #Read the file

    while(True):
        try:

            ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name) + '.txt') #Read the file
    
            ArrayAux = np.array_split(ArrayFile, numeroCarateristicas) #Split the array into 4 or 5 (numeroCaraterísticas) and place them into a new numpy.array


            #Place data in each list and turning numpy.array into list
            #moisture_list = ArrayAux[0].tolist() 
            #temperature_list = ArrayAux[1].tolist()
            conductivity_list = ArrayAux[0].tolist()
            salinity_list = ArrayAux[1].tolist()
            time_list = ArrayAux[2].tolist()

            #Remove the first in list
            #moisture_list.pop(0)
            #temperature_list.pop(0)
            conductivity_list.pop(0)
            salinity_list.pop(0)
            time_list.pop(0)

            #Turn array values from str into int
            #moisture_list = list(map(float,moisture_list))
            #temperature_list = list(map(float,temperature_list))
            conductivity_list = list(map(float,conductivity_list))
            salinity_list = list(map(float,salinity_list))
            time_list = list(map(float,time_list))


        

            curva.setData(time_list, conductivity_list)
            QtGui.QApplication.processEvents()
            time.sleep(6)
    
        except Exception as e:
            pass

except Exception as e:
    print("No hay ningun archivo con ese nombre")
