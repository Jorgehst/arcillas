import xlsxwriter
import numpy as np
import os
import errno
import shutil

moisture_list = []
temperature_list =  []
conductivity_list = []
salinity_list = []
time_list = []
numeroCarateristicas = 5

def readFile(fileName):
        fileData = open(fileName, "r")
        data_list = fileData.read().splitlines()
        fileData.close()
        return data_list


def main():

    try:
        file_name = input("Introduzca el nombre del archivo: ") 
        try:
                os.mkdir('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name))
        except OSError as e:
                if e.errno != errno.EEXIST:
                        raise


        def readFile(fileName):
                fileData = open(fileName, "r")
                data_list = fileData.read().splitlines()
                fileData.close()
                return data_list


        ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name) + '.txt')
        ArrayAux = np.array_split(ArrayFile, numeroCarateristicas)
        moisture_list = ArrayAux[0].tolist() 
        temperature_list = ArrayAux[1].tolist()
        conductivity_list = ArrayAux[2].tolist()
        salinity_list = ArrayAux[3].tolist()
        if numeroCarateristicas == 5:
                time_list = ArrayAux[4].tolist()

        moisture_list.pop(0)
        temperature_list.pop(0)
        conductivity_list.pop(0)
        salinity_list.pop(0)
        if numeroCarateristicas == 5:
                time_list.pop(0)


        print(moisture_list)
        print(temperature_list)
        print(conductivity_list)
        print(salinity_list)
        if numeroCarateristicas == 5:
                print(time_list)

        
        workbook = xlsxwriter.Workbook('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/' + str(file_name) + '.xlsx')

        bold_format = workbook.add_format({'bold': True})
        bold_format.set_align('center')

        cell_format = workbook.add_format()
        cell_format.set_text_wrap()
        cell_format.set_align('center')


        worksheet = workbook.add_worksheet('Test1')
        worksheet.set_column('A:D', 18)

        worksheet.write('A1', 'Moisture (%)', bold_format)
        worksheet.write('B1', 'Temperature (grados C)', bold_format)
        worksheet.write('C1', 'Conductivity (us/cm)', bold_format)
        worksheet.write('D1', 'Salinity (ppm)', bold_format)
        worksheet.write('E1', 'Time (s)', bold_format)


        def writeInFile(Column, Data):
            rowIndex = 2
            for i in Data:
                worksheet.write(str(Column)+str(rowIndex), i, cell_format)
                rowIndex += 1

        writeInFile('A', moisture_list)
        writeInFile('B', temperature_list)
        writeInFile('C', conductivity_list)
        writeInFile('D', salinity_list)
        writeInFile('E', time_list)

        workbook.close()
    except Exception as e:
        shutil.rmtree('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name))
        print("No existe ningun archivo con ese nombre")

if __name__ == "__main__":
    main()

