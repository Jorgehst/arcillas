import tkinter as tk
from PIL import Image, ImageTk #Para el logo
from tkinter.filedialog import askopenfile
from matplotlib.figure import Figure
import xlsxwriter
import numpy as np
import os
from os import listdir
from os.path import isfile, join
import errno
import shutil
import pandas as pd
import matplotlib.pyplot as plt
import subprocess

ruta = "/home/howlabuser/Escritorio/DatosRecibidos"


numeroCarateristicas = 3
i=0

def readFile(fileName):
        fileData = open(fileName, "r")
        data_list = fileData.read().splitlines()
        fileData.close()
        return data_list

def startMedicion():
        
	start.set("En ejecucion...")
	subprocess.call('/home/howlabuser/InicioMedidorArcilla.bash')
	start.set("Iniciar")
        

def sumaLista(Lista):
        valor =0
        for i in Lista:
                valor = valor + i
        return valor

def listarDirectorio():
        listar.set("Listando...")
        text_box2.config(state=tk.NORMAL)
        text_box2.delete("1.0", "end")
        archivos =[a for a in listdir(ruta) if isfile(join(ruta, a))]
        for text in archivos:
                text_box2.insert(1.0, "  " +str(text)+ " / ")
        listar.set("Listar ficheros")
        text_box2.config(state=tk.DISABLED)



def graficar():

        graph.set("Creando graficas...")
        text_box3.config(state=tk.NORMAL)
        text_box3.delete("1.0", "end")

        try:
                file_name1 = input("Introduzca el nombre del archivo: ")
                #Creo directorio 
                try:
                        #os.mkdir('C:/Users/jorge/Desktop/Interfaces/' + str(file_name))
                        os.mkdir('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1))
                except OSError as e:
                        if e.errno != errno.EEXIST:
                                raise

                #ArrayFile = readFile('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + '.txt') #Read the file                  
                ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name1) + '.txt') #Read the file
                ArrayAux = np.array_split(ArrayFile, numeroCarateristicas) #Split the array into 4 or 5 (numeroCaraterísticas) and place them into a new numpy.array


                #Place data in each list and turning numpy.array into list
                conductivity_list = ArrayAux[0].tolist()
                salinity_list = ArrayAux[1].tolist()
                time_list = ArrayAux[2].tolist()

                conductivity_list.pop(0)
                salinity_list.pop(0)
                time_list.pop(0)


                #ArrayFile = readFile('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + 'InicialFinal.txt')
                ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name1) + 'InicialFinal.txt')
                ArrayAux = np.array_split(ArrayFile, 8)
                moisture_listI = ArrayAux[0].tolist() 
                temperature_listI = ArrayAux[1].tolist()
                conductivity_listI = ArrayAux[2].tolist()
                salinity_listI = ArrayAux[3].tolist()

                moisture_listF = ArrayAux[4].tolist() 
                temperature_listF = ArrayAux[5].tolist()
                conductivity_listF = ArrayAux[6].tolist()
                salinity_listF = ArrayAux[7].tolist()


                moisture_listI.pop(0)
                temperature_listI.pop(0)
                conductivity_listI.pop(0)
                salinity_listI.pop(0)

                moisture_listF.pop(0)
                temperature_listF.pop(0)
                conductivity_listF.pop(0)
                salinity_listF.pop(0)


                #Turn array values from str into float
                moisture_listI = list(map(float,moisture_listI))
                temperature_listI = list(map(float,temperature_listI))
                conductivity_listI = list(map(float,conductivity_listI))
                salinity_listI = list(map(float,salinity_listI))
                moisture_listF = list(map(float,moisture_listF))
                temperature_listF = list(map(float,temperature_listF))
                conductivity_listF = list(map(float,conductivity_listF))
                salinity_listF = list(map(float,salinity_listF))
                
                conductivity_list = list(map(float,conductivity_list))
                salinity_list = list(map(float,salinity_list))
                time_list = list(map(float,time_list))

                moistureI = sumaLista(moisture_listI)/5
                temperatureI = sumaLista(temperature_listI)/5
                conductivityI = sumaLista(conductivity_listI)/5
                salinityI =sumaLista(conductivity_listI)/5

                moistureF = sumaLista(moisture_listF)/5
                temperatureF = sumaLista(temperature_listF)/5
                conductivityF = sumaLista(conductivity_listF)/5
                salinityF =sumaLista(conductivity_listF)/5

                firstTime = time_list[0]
                lastTime = time_list[-1] #Accedo al ultimo elemento de la lista

                #Graficando frente al tiempo
                plt.plot(time_list, conductivity_list, 'b')
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("Conductivity (us/cm)")   # Inserta el título del eje Y
                plt.grid(True)
                plt.title("Conductivity")
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1) + '/Conductivity_Time.png')

                plt.plot(time_list, salinity_list, 'k')
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("Salinity (ppm)")   # Inserta el título del eje Y
                plt.grid(True)
                plt.title("Salinity")
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1) + '/Salinity_Time.png')

                



               
                #All in one
                
                plt.figure(figsize=(10,10))
                plt.xlim(-2, (time_list[-1]+2))
                plt.ylim(-1, (conductivity_list[0]+50))
                plt.plot(time_list, conductivity_list)
                plt.title("All in one")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("ordenada")   # Inserta el título del eje Y
                plt.grid(True)
                plt.ion()   # Activa modo interactivo de dibujo
                plt.plot(firstTime, temperatureI)
                plt.plot(lastTime, temperatureF)
                plt.plot(firstTime, moistureI)
                plt.plot(lastTime, moistureF)
                plt.plot(firstTime, conductivityI)
                plt.plot(lastTime, conductivityF)
                plt.plot(firstTime, salinityI)
                plt.plot(lastTime, salinityF)
                plt.plot(time_list, salinity_list)
                plt.ioff()  # Desactiva modo interactivo de dibujo
                plt.plot(firstTime, moistureI, marker="o", color='g', label = "Moisture InitFinal(%)")
                plt.plot(firstTime, temperatureI, marker="o", color='r', label = "Temperature InitFinal(ºC)")
                plt.plot(firstTime, conductivityI, marker="o", color='m', label = "Conductivity InitFinal(us/cm)")
                plt.plot(firstTime, salinityI, marker="o", color='y', label = "Salinity InitFinal(ppm)")
                plt.plot(lastTime, moistureF, marker="o", color='g')
                plt.plot(lastTime, temperatureF, marker="o", color='r')
                plt.plot(lastTime, conductivityF, marker="o", color='m')
                plt.plot(lastTime, salinityF, marker="o", color='y')
                plt.plot(time_list, conductivity_list, linestyle='--', color='b', label = "Conductivity (us/cm)")
                plt.plot(time_list, salinity_list, linestyle='--', color='k', label = "Salinity (ppm)")
                plt.legend(loc="upper right", bbox_to_anchor = (1.8, 1.0))
                #plt.savefig('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + '/AllinOne_Time.png')
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1) + '/AllinOne_Time.png')
                #plt.show()

                #Ploting in same window
                plt.figure(figsize=(20,10))
                plt.subplot(2,1,1)
                plt.plot(conductivity_list, 'b')
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("Conductivity (us/cm)")   # Inserta el título del eje Y
                plt.grid(True)

                plt.subplot(2,1,2)
                plt.plot(salinity_list, 'k')
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("Salinity (ppm)")   # Inserta el título del eje Y
                plt.grid(True)
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1) + '/ConductivityAndSalinity_Time.png')
                print("Graficas "+str(file_name1) + " creadas con exito")
                text_box3.insert(1.0, "Graficas "+str(file_name1) + " creadas con exito")
                
        except Exception as e:
                #shutil.rmtree('C:/Users/jorge/Desktop/Interfaces/' + str(file_name))
                shutil.rmtree('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name1))
                text_box3.insert(1.0, str(file_name1)+" no existe")
                print("No existe ningún archivo con ese nombre")

        graph.set("Crear graficas")
        text_box3.config(state=tk.DISABLED)



def CreateExcel():
    
    
    excel.set("Creando Excel...")
    text_box1.config(state=tk.NORMAL)
    text_box1.delete(1.0, "end")

    try:
        file_name2 = input("Introduzca el nombre del archivo: ")

        try:
                #os.mkdir('C:/Users/jorge/Desktop/Interfaces/' + str(file_name))
                os.mkdir('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name2))
        except OSError as e:
                if e.errno != errno.EEXIST:
                        raise


        #ArrayFile = readFile('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + '.txt')
        ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name2) + '.txt')
        ArrayAux = np.array_split(ArrayFile, numeroCarateristicas)

        conductivity_listE = ArrayAux[0].tolist()
        salinity_listE = ArrayAux[1].tolist()
        time_listE = ArrayAux[2].tolist()

        conductivity_listE.pop(0)
        salinity_listE.pop(0)
        time_listE.pop(0)


        print(conductivity_listE)
        print(salinity_listE)
        print(time_listE)

        #ArrayFile = readFile('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + 'InicialFinal.txt')
        ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name2) + 'InicialFinal.txt')
        ArrayAux = np.array_split(ArrayFile, 8)
        moisture_listEI = ArrayAux[0].tolist() 
        temperature_listEI = ArrayAux[1].tolist()
        conductivity_listEI = ArrayAux[2].tolist()
        salinity_listEI = ArrayAux[3].tolist()

        moisture_listEF = ArrayAux[4].tolist() 
        temperature_listEF = ArrayAux[5].tolist()
        conductivity_listEF = ArrayAux[6].tolist()
        salinity_listEF = ArrayAux[7].tolist()


        moisture_listEI.pop(0)
        temperature_listEI.pop(0)
        conductivity_listEI.pop(0)
        salinity_listEI.pop(0)

        moisture_listEF.pop(0)
        temperature_listEF.pop(0)
        conductivity_listEF.pop(0)
        salinity_listEF.pop(0)



        print(moisture_listEI)
        print(temperature_listEI)
        print(conductivity_listEI)
        print(salinity_listEI)
        print(moisture_listEF)
        print(temperature_listEF)
        print(conductivity_listEF)
        print(salinity_listEF)
 

        #workbook = xlsxwriter.Workbook('C:/Users/jorge/Desktop/Interfaces/' + str(file_name) + '/' + str(file_name) + '.xlsx')
        workbook = xlsxwriter.Workbook('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name2) + '/' + str(file_name2) + '.xlsx')

        bold_format = workbook.add_format({'bold': True})
        bold_format.set_align('center')

        cell_format = workbook.add_format()
        cell_format.set_text_wrap()
        cell_format.set_align('center')


        worksheet = workbook.add_worksheet('Test1')
        worksheet.set_column('A:L', 20)


        worksheet.write('A1', 'Conductivity (us/cm)', bold_format)
        worksheet.write('B1', 'Salinity (ppm)', bold_format)
        worksheet.write('C1', 'Time (s)', bold_format)

        worksheet.write('E1', 'Moisture Inicial(%)', bold_format)
        worksheet.write('F1', 'Temp Inicial(grados C)', bold_format)
        worksheet.write('G1', 'Cond Inicial(us/cm)', bold_format)
        worksheet.write('H1', 'Salinity Inicial(ppm)', bold_format)
        worksheet.write('I1', 'Moisture Final(%)', bold_format)
        worksheet.write('J1', 'Temp Final(grados C)', bold_format)
        worksheet.write('K1', 'Cond Final(us/cm)', bold_format)
        worksheet.write('L1', 'Salinity Final(ppm)', bold_format)


        def writeInFile(Column, Data):
            rowIndex = 2
            for i in Data:
                worksheet.write(str(Column)+str(rowIndex), i, cell_format)
                rowIndex += 1

        
        writeInFile('A', conductivity_listE)
        writeInFile('B', salinity_listE)
        writeInFile('C', time_listE)

        writeInFile('E', moisture_listEI)
        writeInFile('F', temperature_listEI)
        writeInFile('G', conductivity_listEI)
        writeInFile('H', salinity_listEI)

        writeInFile('I', moisture_listEF)
        writeInFile('J', temperature_listEF)
        writeInFile('K', conductivity_listEF)
        writeInFile('L', salinity_listEF)

        workbook.close()
        text_box1.insert(1.0, "Excel "+str(file_name2) + " creado con exito")
        print("Excel "+str(file_name2) + " creado con exito")
    except Exception as e:
        
        #shutil.rmtree('C:/Users/jorge/Desktop/Interfaces/' + str(file_name))
        shutil.rmtree('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name2))
        text_box1.insert(1.0, str(file_name2)+" no existe")
        print("No existe ningun archivo con ese nombre")

    excel.set("Crear Excel")
    text_box1.config(state=tk.DISABLED)


root = tk.Tk()
root.wm_title("Medicion Arcilla")

canvas = tk.Canvas(root, width = 500, height = 300) #Ventana, ancho, alto
canvas.grid(columnspan=3, rowspan=7) #separa la interfaz en 3 columnas para situar con mayor precision

#Logo
logo = Image.open('/home/howlabuser/How.png')
logo = ImageTk.PhotoImage(logo) #transformamos el logo en na imagen tkinter
logo_label = tk.Label(image=logo)#Lo ponemos en un "label widget"
logo_label.image = logo #para quefncione lo anterior
logo_label.grid(column=1, row=0) #situamos el logo

#Titulo
instructions= tk.Label(root, text="TOMA DE DATOS EN FILTRO PRENSA", font="Raleway")
instructions.grid(column=1, row=1)

#start button
start = tk.StringVar()
start_btn = tk.Button(root, textvariable=start, command=startMedicion, font="Raleway", bg='#20bebe', fg='white', height=2, width=15)
start.set("Iniciar")
start_btn.grid(column=1, row=2)





#excel button
excel = tk.StringVar()
excel_btn = tk.Button(root, textvariable=excel, command=CreateExcel, font="Raleway", bg='#20bebe', fg='white', height=2, width=15)
excel.set("Crear Excel")
excel_btn.grid(column=0, row=5)

text_box1 = tk.Text(root,height=0.5, width= 30, padx=15, pady=15, state=tk.DISABLED)
text_box1.grid(column=0,row=6)


#list button
listar = tk.StringVar()
listar_btn = tk.Button(root, textvariable=listar, command=listarDirectorio, font="Raleway", bg='#20bebe', fg='white', height=2, width=15)
listar.set("Listar ficheros")
listar_btn.grid(column=1, row=5)

text_box2 = tk.Text(root,height=15, width= 30, padx=15, pady=15, state=tk.DISABLED)
text_box2.grid(column=1,row=6)

#graph button
graph = tk.StringVar()
graph_btn = tk.Button(root, textvariable=graph, command=graficar, font="Raleway", bg='#20bebe', fg='white', height=2, width=15)
graph.set("Crear graficas")
graph_btn.grid(column=2, row=5)


text_box3 = tk.Text(root,height=0.5, width= 30, padx=15, pady=15, state=tk.DISABLED)
text_box3.grid(column=2,row=6)

canvas = tk.Canvas(root, width = 500, height = 70) #Ventana, ancho, alto
canvas.grid(columnspan=3) #separa la interfaz en 3 columnas para situar con mayor precision


root.mainloop()
