#pip install pandas
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import errno
import shutil

moisture_list = []
temperature_list =  []
conductivity_list = []
salinity_list = []
numeroMuestras = []
#Introucir el número de características
numeroCarateristicas = 5
i = 0
try:
        file_name = input("Introzca el nombre del archivo: ")
        #Creo directorio 
        try:
                os.mkdir('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name))
        except OSError as e:
                if e.errno != errno.EEXIST:
                        raise


        def readFile(fileName):
                fileData = open(fileName, "r") #opens the file in read mode
                data_list = fileData.read().splitlines() #puts the file into an array
                fileData.close()
                return data_list


        ArrayFile = readFile('/home/howlabuser/Escritorio/DatosRecibidos/' + str(file_name) + '.txt') #Read the file
        ArrayAux = np.array_split(ArrayFile, numeroCarateristicas) #Split the array into 4 or 5 (numeroCaraterísticas) and place them into a new numpy.array


        #Place data in each list and turning numpy.array into list
        moisture_list = ArrayAux[0].tolist() 
        temperature_list = ArrayAux[1].tolist()
        conductivity_list = ArrayAux[2].tolist()
        salinity_list = ArrayAux[3].tolist()
        if numeroCarateristicas == 5:
                time_list = ArrayAux[4].tolist()

        #Remove the first in list
        moisture_list.pop(0)
        temperature_list.pop(0)
        conductivity_list.pop(0)
        salinity_list.pop(0)
        if numeroCarateristicas == 5:
                time_list.pop(0)

        #Turn array values from str into int
        moisture_list = list(map(float,moisture_list))
        temperature_list = list(map(float,temperature_list))
        conductivity_list = list(map(float,conductivity_list))
        salinity_list = list(map(float,salinity_list))
        if numeroCarateristicas == 5:
                time_list = list(map(float,time_list))

        while (i < len(moisture_list)):
                numeroMuestras.append(i)
                i += 1

        print(moisture_list)
        print(temperature_list)
        print(conductivity_list)
        print(salinity_list)
        if numeroCarateristicas == 5:
                print(time_list)

        #Ploting each list in a single window

        plt.plot(moisture_list, linestyle=':', color='g')
        plt.title("Moisture Evolution")   # Establece el título del gráfico
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.ylabel("Moisture (%)")   # Establece el título del eje y
        plt.grid(True)
        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Moisture_Sample.png')
        plt.show()


        plt.plot(temperature_list, linestyle=':', color='r')
        plt.title("Temperature Evolution")   # Establece el título del gráfico
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.ylabel("Temperature (ºC)")   # Establece el título del eje y
        plt.grid(True)
        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Temperature_Sample.png')
        plt.show()

        plt.plot(conductivity_list, linestyle='--', color='b')
        plt.title("Conductivity Evolution")   # Establece el título del gráfico
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.ylabel("Conductivity (us/cm)")   # Establece el título del eje y
        plt.grid(True)
        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Conductivity_Sample.png')
        plt.show()

        plt.plot(salinity_list, linestyle='--', color='y')
        plt.title("Salinity Evolution")   # Establece el título del gráfico
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.ylabel("Salinity (ppm)")   # Establece el título del eje y
        plt.grid(True)
        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Salinity_Sample.png')
        plt.show()

        #Ploting in same window (diferent plots)

        plt.subplot(2,2,1)
        plt.plot(moisture_list, linestyle='--', color='g')
        plt.title("Moisture Evolution")   # Establece el título del gráfico
        plt.ylabel("Moisture (%)")   # Establece el título del eje y
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.grid(True)

        plt.subplot(2,2,2)
        plt.plot(temperature_list, linestyle='--', color='r')
        plt.title("Temperature Evolution")   # Establece el título del gráfico
        plt.ylabel("Temperature (ºC)")   # Establece el título del eje y
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.grid(True)

        plt.subplot(2,2,3)
        plt.plot(conductivity_list, linestyle='--', color='b')
        plt.title("Conductivity Evolution")   # Establece el título del gráfico
        plt.ylabel("Conductivity (us/cm)")   # Establece el título del eje y
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.grid(True)

        plt.subplot(2,2,4)
        plt.plot(salinity_list, linestyle='--', color='y')
        plt.title("Salinity Evolution")   # Establece el título del gráfico
        plt.ylabel("Salinity (ppm)")   # Establece el título del eje y
        plt.xlabel("Número de muestra")   # Establece el título del eje x
        plt.grid(True)

        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/SameWindow_Sample.png')

        plt.show()



        #All in one

        plt.plot(moisture_list)
        plt.title("All in one")   # Establece el título del gráfico
        plt.xlabel("Número de muestra")   # Inserta el título del eje X
        plt.ylabel("ordenada")   # Inserta el título del eje Y
        plt.grid(True)
        plt.ion()   # Activa modo interactivo de dibujo
        plt.plot(temperature_list)
        plt.plot(conductivity_list)
        plt.plot(salinity_list)
        plt.ioff()  # Desactiva modo interactivo de dibujo
        plt.plot(moisture_list, linestyle='--', color='g', label = "Moisture (%)")
        plt.plot(temperature_list, linestyle='--', color='r', label = "Temperature (ºC)")
        plt.plot(conductivity_list, linestyle='--', color='b', label = "Conductivity (us/cm)")
        plt.plot(salinity_list, linestyle='--', color='y', label = "Salinity (ppm)")
        plt.legend(loc="upper left")
        plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/AllinOne_Sample.png')
        plt.show()
        #--------------------------------------------------------------------------------------------------------
        #Graficando frente al tiempo
        if numeroCarateristicas == 5:
                plt.plot(time_list, moisture_list, linestyle=':', color='g')
                plt.title("Moisture Evolution")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.ylabel("Moisture (%)")   # Establece el título del eje y
                plt.grid(True)
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Moisture_Time.png')
                plt.show()

                plt.plot(time_list, temperature_list, linestyle=':', color='r')
                plt.title("Temperature Evolution")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.ylabel("Temperature (ºC)")   # Establece el título del eje y
                plt.grid(True)
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Remperature_Time.png')
                plt.show()

                plt.plot(time_list, conductivity_list, linestyle='--', color='b')
                plt.title("Conductivity Evolution")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.ylabel("Conductivity (us/cm)")   # Establece el título del eje y
                plt.grid(True)
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Conductivity_Time.png')
                plt.show()

                plt.plot(time_list, salinity_list, linestyle='--', color='y')
                plt.title("Salinity Evolution")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.ylabel("Salinity (ppm)")   # Establece el título del eje y
                plt.grid(True)
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/Salinity_Time.png')
                plt.show()

                #Ploting in same window (diferent plots)

                plt.subplot(2,2,1)
                plt.plot(time_list, moisture_list, linestyle='--', color='g')
                plt.title("Moisture Evolution")   # Establece el título del gráfico
                plt.ylabel("Moisture (%)")   # Establece el título del eje y
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.grid(True)

                plt.subplot(2,2,2)
                plt.plot(time_list, temperature_list, linestyle='--', color='r')
                plt.title("Temperature Evolution")   # Establece el título del gráfico
                plt.ylabel("Temperature (ºC)")   # Establece el título del eje y
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.grid(True)

                plt.subplot(2,2,3)
                plt.plot(time_list, conductivity_list, linestyle='--', color='b')
                plt.title("Conductivity Evolution")   # Establece el título del gráfico
                plt.ylabel("Conductivity (us/cm)")   # Establece el título del eje y
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.grid(True)

                plt.subplot(2,2,4)
                plt.plot(time_list, salinity_list, linestyle='--', color='y')
                plt.title("Salinity Evolution")   # Establece el título del gráfico
                plt.ylabel("Salinity (ppm)")   # Establece el título del eje y
                plt.xlabel("Tiempo (s)")   # Establece el título del eje x
                plt.grid(True)

                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/SameWindow_Time.png')

                plt.show()



                #All in one

                plt.plot(time_list, moisture_list)
                plt.title("All in one")   # Establece el título del gráfico
                plt.xlabel("Tiempo (s)")   # Inserta el título del eje X
                plt.ylabel("ordenada")   # Inserta el título del eje Y
                plt.grid(True)
                plt.ion()   # Activa modo interactivo de dibujo
                plt.plot(time_list, temperature_list)
                plt.plot(time_list, conductivity_list)
                plt.plot(time_list, salinity_list)
                plt.ioff()  # Desactiva modo interactivo de dibujo
                plt.plot(time_list, moisture_list, linestyle='--', color='g', label = "Moisture (%)")
                plt.plot(time_list, temperature_list, linestyle='--', color='r', label = "Temperature (ºC)")
                plt.plot(time_list, conductivity_list, linestyle='--', color='b', label = "Conductivity (us/cm)")
                plt.plot(time_list, salinity_list, linestyle='--', color='y', label = "Salinity (ppm)")
                plt.legend(loc="upper left")
                plt.savefig('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name) + '/AllinOne_Time.png')
                plt.show()
except Exception as e:
        shutil.rmtree('/home/howlabuser/Escritorio/AnalisisDatos/' + str(file_name))
        print("No existe ningún archivo con ese nombre")

"""
df = pd.DataFrame()
df['Muestra Nº'] = numeroMuestras
df['Moisture (%)'] = moisture_list
df['Temperature (ºC)'] = temperature_list
df['Conductivity (us/cm)'] = conductivity_list
df['Salinity (ppm)'] = salinity_list

#print(df)
x = df['Muestra Nº']
y = df['Moisture (%)']
"""





