#coding=utf-8
import os
import time
from ftplib import FTP
from os import listdir
from os.path import isfile, join

#Defino parametros necesarios para cliente FTP
ip = '192.168.3.140'
user = 'howlabuser'
password = 'howlab'

#Conexion con servidor FTP
ftp = FTP(ip, user, password)

ruta = "/root/DataUpload"

#Download from FTP
def downloadFTP(OriginFile, DestinyFile):
    try:
        ftp.retrbinary("/home/howlabuser/"+str(OriginFile), open("/root/DataUpload/"+str(DestinyFile), "wb").write)
    except Exception as e:
        pass

def listarDirectorio():
    print("Archivos almacenados: ")
    archivos =[a for a in listdir(ruta) if isfile(join(ruta, a))]
    for text in archivos:
        print(text)

#Upload to FTP
def uploadFTP(OriginFile, DestinyFile):
    try:
        ftp.storbinary("STOR "+"/Escritorio/DatosRecibidos/"+str(DestinyFile), open("/root/DataUpload/"+str(OriginFile), "rb"))
    except Exception as e:
        pass

try:

    listarDirectorio()

    file_name = input("Introduzca el nombre del archivo: ")
    #Creo directorio 

    file_name_complete1 = str(file_name) +".txt"
    file_name_complete2 = str(file_name) +"InicialFinal.txt"

    try:
        ftp = FTP(ip, user, password)
        time.sleep(0.1)
        uploadFTP(file_name_complete1, file_name_complete1)
        time.sleep(0.1)
        uploadFTP(file_name_complete2, file_name_complete2)
    except Exception as e:
        print("Fallo al enviar al servidor")

except Exception as e:
    print("No existe ningún archivo con ese nombre")