#coding=utf-8
from pymodbus.client.sync import ModbusSerialClient as ModbusClient 
import os
import time
from ftplib import FTP
from os.path import isfile, join
from os import listdir

#Defino ruta interna para guardar archivo
save_path = None
file_name = None
file_name_complete = None

#Defino parametros necesarios para cliente FTP
ip = '192.168.3.140'
user = 'howlabuser'
password = 'howlab'

#Conexion con servidor FTP
ftp = FTP(ip, user, password)

ruta= "/root/DataUpload/"

#Defino listas donde se almacenarán los valores leidos
data_list_moisture_Init=[]
data_list_temperature_Init=[]
data_list_EC_Init=[]
data_list_salinity_Init=[]
data_list_salinity_Init=[]
data_list_moisture_Final=[]
data_list_temperature_Final=[]
data_list_EC_Final=[]
data_list_salinity_Final=[]
data_list_EC=[]
data_list_salinity=[]
ListaTiempo = []


def mem_address():
    mem_Read = {
        'soil moisture' : 0,
        'soil temperature' : 1,
        'soil EC' : 2,
        'salinity' : 3,
    }

    mem_ReadAndWrite = {
        'Conductance temperature coefficient' : 40035,
        'Salinity coefficient' : 40036,
        'TDS coefficient' : 40037,
        'Temperature calibration value' : 40081,
        'Calibration value of water content' : 40082,
        'Conductivity calibration value' : 40083,
        'Device address' : 42001,
        'Device baud rate' : 42002
    }

    default_config = {
        'method' : 'rtu',
        'bitstop' : 1,
        'bytesize' : 8,
        'parity' : 'N',
        'baudrate' : 9600,
        'timeout' : 2,
        'default address' : '01' 
    }
    
    return {
        'mem_Read': mem_Read,
        'mem_ReadAndWrite' : mem_ReadAndWrite,
        'default_config' : default_config
    }


def addToDicEC(EC, salinity):
    measure = {}
    measure['conductividad'] = EC
    measure['salinidad'] = salinity
    return(measure)

#Download from FTP
def downloadFTP(OriginFile, DestinyFile):
    try:
        ftp.retrbinary("/home/howlabuser/"+str(OriginFile), open("/root/DataUpload/"+str(DestinyFile), "wb").write)
    except Exception as e:
        pass

#Upload to FTP
def uploadFTP(OriginFile, DestinyFile):
    try:
        ftp.storbinary("STOR "+"/Escritorio/DatosRecibidos/"+str(DestinyFile), open("/root/DataUpload/"+str(OriginFile), "rb"))
    except Exception as e:
        pass

def listarDirectorio():
        print("Lista de archivos: ")
        archivos =[a for a in listdir(ruta) if isfile(join(ruta, a))]
        for text in archivos:
            print("  " +str(text)+ " / ")
  



def main():
    #Defino los parametros de la conexión Modbus RTU
    method = mem_address()['default_config']['method']
    stopbits = mem_address()['default_config']['bitstop']
    bytesize = mem_address()['default_config']['bytesize']
    parity = mem_address()['default_config']['parity']
    baudrate = mem_address()['default_config']['baudrate']
    timeout = mem_address()['default_config']['timeout']
    port = '/dev/ttyUSB0'

    client = ModbusClient(
    method=method,
    stopbits=stopbits,
    bytesize=bytesize,
    parity=parity,
    baudrate=baudrate,
    timeout=timeout,
    port=port)

    #To check if data have been introduced correctly
    nameOK = 0
    intervalOK = 0

    #Sensor connection
    connection = client.connect()
    file_name = ""
    

    InitLoop = 0
    #Valor para tomar de forma exitosa los datos finales
    ErrorFinal = 0
    if connection:

        listarDirectorio()
        while (file_name == ""):
            file_name =str(input("Inserte nombre del archivo donde se guardaran los datos: "))

        input("Inserte el sensor en la arcilla para obtener parametro de humedad FINAL y  pulse ENTER...")
        InitLoop = 0
        while (InitLoop < 5):
            if connection:
                print("_____________________________________________________")
                print("mem_Read values")
                for key, value in mem_address()['mem_Read'].items():
                    rr = client.read_holding_registers(value, 4, unit=0x01)
                    if not rr.isError():
                        val = rr.registers[0]
                        if(key == 'soil moisture'):
                            aux1 = float(val)
                            aux1 = aux1/10
                            data_list_moisture_Final.append(aux1)
                            print("Moisture: " + str(data_list_moisture_Final))
                        elif(key == 'soil temperature'):
                            aux = float(val)
                            aux = aux/10
                            data_list_temperature_Final.append(aux)
                        
                            print("Temperature: " + str(data_list_temperature_Final))
                        elif(key == 'soil EC'):
                            data_list_EC_Final.append(val)
                            print("EC:" + str(data_list_EC_Final))
                        else:
                            data_list_salinity_Final.append(val)
                            print("Salinity: " + str(data_list_salinity_Final))
                    else:
                        print('{}: error en lectura'.format(key))
                    time.sleep(1)
            InitLoop += 1

        if ((len(data_list_moisture_Final) != 5) or (len(data_list_temperature_Final) != 5) or (len(data_list_EC_Final) != 5) or (len(data_list_salinity_Final) != 5)):
            ErrorFinal = 1
            print("Error en la toma final de datos de datos")

        while (ErrorFinal == 1):
            input("Introduzca adecuadamente el sensor y pulse ENTER para continuar...")
            InitLoop = 0
            #Empty the list
            data_list_moisture_Final.clear()
            data_list_temperature_Final.clear()
            data_list_EC_Final.clear()
            data_list_salinity_Final.clear()
            while (InitLoop < 5):
                if connection:
                    print("_____________________________________________________")
                    print("mem_Read values")
                    for key, value in mem_address()['mem_Read'].items():
                        rr = client.read_holding_registers(value, 4, unit=0x01)
                        if not rr.isError():
                            val = rr.registers[0]
                            if(key == 'soil moisture'):
                                aux1 = float(val)
                                aux1 = aux1/10
                                data_list_moisture_Final.append(aux1)
                                print("Moisture: " + str(data_list_moisture_Final))
                            elif(key == 'soil temperature'):
                                aux = float(val)
                                aux = aux/10
                                data_list_temperature_Final.append(aux)
                            
                                print("Temperature: " + str(data_list_temperature_Final))
                            elif(key == 'soil EC'):
                                data_list_EC_Final.append(val)
                                print("EC:" + str(data_list_EC_Final))
                            else:
                                data_list_salinity_Final.append(val)
                                print("Salinity: " + str(data_list_salinity_Final))
                        else:
                            print('{}: error en lectura'.format(key))
                        time.sleep(1)
                InitLoop += 1
            if ((len(data_list_moisture_Final) == 5) and (len(data_list_temperature_Final) == 5) and (len(data_list_EC_Final) == 5) and (len(data_list_salinity_Final) == 5)):
                ErrorFinal = 0
            else :
                print("Error en la toma final de datos de datos")



        #Se guardan los datos en un fichero y se envían a un servidor FTP
        save_path = '/root/DataUpload'
        file_name_complete = str(file_name) +"InicialFinal.txt"
        if (save_path != None and file_name_complete != None):
            completeName = os.path.join(save_path, file_name_complete)
            #print(completeName)

            with open(completeName, "a") as f:
                f.write("%s\n" % "Humedad Final (%)" )
                for item in data_list_moisture_Final:
                    f.write("%s\n" % item)
                f.write("%s\n" % "Temperatura Final (grados C)" )
                for item in data_list_temperature_Final:
                    f.write("%s\n" % item)
                f.write("%s\n" % "Conductividad Final (us/cm)" )
                for item in data_list_EC_Final:
                    f.write("%s\n" % item)
                f.write("%s\n" % "Salinidad Final (ppm)" )
                for item in data_list_salinity_Final:
                    f.write("%s\n" % item)
                f.close()
                
                try:
                    ftp = FTP(ip, user, password)
                    time.sleep(0.1)
                    uploadFTP(file_name_complete, file_name_complete)
                except Exception as e:
                    print("Fallo al enviar al servidor")
                

        else:
            print("Error al guardar los datos")
     
            
    print("Fin de ejecución de programa")

if __name__ == "__main__":
    main()